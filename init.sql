CREATE DATABASE IF NOT EXISTS TaskManagementApp;

USE TaskManagementApp;

CREATE TABLE IF NOT EXISTS user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    role VARCHAR(25) DEFAULT 'user'
);

CREATE TABLE IF NOT EXISTS task (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    status VARCHAR(25) DEFAULT 'todo',
    priority VARCHAR(25) DEFAULT 'low',
    user_id INT,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    due_date DATE,
    FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS history (
    id INT AUTO_INCREMENT PRIMARY KEY,
    task_id INT,
    status VARCHAR(25) DEFAULT 'todo',
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (task_id) REFERENCES task(id)
);

CREATE TABLE IF NOT EXISTS notification (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    message TEXT,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES user(id)
);


INSERT INTO `user` (`created_at`, `email`, `id`, `name`, `password`, `role`)
VALUES (CURRENT_TIMESTAMP, 'admin@gmail.com', 1, 'mo', '$2a$10$95D/Wmu9Lm5xCcNJ/wlz5O7u6dT991LTfO5wf/hRMGdIohE4DZHVe', 'admin');
