FROM openjdk:17-jdk-slim
WORKDIR /app
COPY . .
RUN apt-get update && apt-get install -y dos2unix && dos2unix /app/mvnw && chmod +x /app/mvnw
RUN ./mvnw clean package
CMD ["java", "-jar", "target/TaskManagement-0.0.1-SNAPSHOT.jar"]