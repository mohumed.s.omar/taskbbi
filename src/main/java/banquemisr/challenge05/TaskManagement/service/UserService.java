package banquemisr.challenge05.TaskManagement.service;

import banquemisr.challenge05.TaskManagement.dto.AddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.AdminAddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.UserDTO;
import banquemisr.challenge05.TaskManagement.entity.User;

import java.util.List;

public interface UserService {

    User getUserByEmail(String email);

    User register(AddUserRequest addUserRequest);


    Integer addUser(AdminAddUserRequest addUserRequest);

    List<UserDTO> getUsers();
}
