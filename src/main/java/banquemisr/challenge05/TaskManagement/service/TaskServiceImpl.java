package banquemisr.challenge05.TaskManagement.service;

import banquemisr.challenge05.TaskManagement.dao.HistoryDAO;
import banquemisr.challenge05.TaskManagement.dao.TaskDAO;
import banquemisr.challenge05.TaskManagement.dto.*;
import banquemisr.challenge05.TaskManagement.entity.*;
import banquemisr.challenge05.TaskManagement.exception.ClientException;
import banquemisr.challenge05.TaskManagement.service.component.EmailComponent;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskDAO taskDAO;
    private final ModelMapper modelMapper;
    private final HistoryDAO historyDAO;
    private final EmailComponent emailComponent;

    @Override
    public Integer addTask(AddTaskRequest addTaskRequest, User user) {
        Task task = modelMapper.map(addTaskRequest, Task.class);
        task.setCreator(user);
        task.setStatus(TaskStatusEnum.todo);

        sendEmailForTaskCreation(user, task);

        return taskDAO.saveTask(task);
    }

    private void sendEmailForTaskCreation(User user, Task task) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDueDate = task.getDueDate().format(formatter);

        emailComponent.sendEmail(
                user.getEmail(),
                "Task Created",
                "Task Created Successfully with due date "
                        + formattedDueDate
                        + " and priority "
                        + task.getPriority()
                        + " and status "
                        + task.getStatus()
        );
    }

    @Override
    public void deleteTask(Integer taskId, User user) {
        var task = taskDAO.findOne(taskId);
        if (task.isEmpty())
            throw new ClientException("Task not found");
        var createdId = task.get().getCreator().getId();
        if (createdId != user.getId())
            throw new ClientException("You are not allowed to delete this task");
        task.ifPresent(taskDAO::deleteTask);
    }

    @Override
    public void updateTask(UpdateTaskRequest updateTaskRequest, Integer taskId, User user) {
        var task = taskDAO.findOne(taskId);
        if (task.isEmpty())
            throw new ClientException("Task not found");
        var createdId = task.get().getCreator().getId();
        if (createdId != user.getId())
            throw new ClientException("You are not allowed to update this task");

        if (task.get().getStatus() != updateTaskRequest.getStatus()) {
            History history = History.builder()
                    .task(task.get())
                    .status(updateTaskRequest.getStatus())
                    .createdAt(LocalDateTime.now())
                    .build();


            historyDAO.addHistory(history);
        }
        task.ifPresent(t -> {
            t.setTitle(updateTaskRequest.getTitle());
            t.setDescription(updateTaskRequest.getDescription());
            t.setDueDate(updateTaskRequest.getDueDate());
            t.setPriority(updateTaskRequest.getPriority());
            t.setStatus(updateTaskRequest.getStatus());
            taskDAO.updateTask(t);
        });
    }


    @Override
    public PaginationTaskResponse getTasks(User user, TaskStatusEnum status,
                                           LocalDateTime fromDueDate, LocalDateTime toDueDate,
                                           TaskPriorityEnum priority, String search,
                                           Integer page, Integer pageSize) {
        var taskList = taskDAO.getTasks(user, status, fromDueDate, toDueDate, priority, search, page, pageSize);

        var taskDTOList = taskList.stream().map(task -> modelMapper.map(task, TaskDTO.class)).toList();

        return PaginationTaskResponse.builder()
                .data(taskDTOList)
                .page(page)
                .pageSize(taskList.size())
                .build();

    }

    @Override
    public List<HistoryDTO> getTaskHistory(Integer taskId, User user) {
        var task = taskDAO.findOne(taskId);
        if (task.isEmpty())
            throw new ClientException("Task not found");
        var createdId = task.get().getCreator().getId();
        if (createdId != user.getId())
            throw new ClientException("You are not allowed to get history for this task");

        var historyList = historyDAO.getHistoryByTask(task.get());

        return historyList.stream().map(history -> modelMapper.map(history, HistoryDTO.class)).toList();
    }


}
