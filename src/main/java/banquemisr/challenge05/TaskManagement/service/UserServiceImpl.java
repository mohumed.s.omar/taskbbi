package banquemisr.challenge05.TaskManagement.service;

import banquemisr.challenge05.TaskManagement.dao.UserDAO;
import banquemisr.challenge05.TaskManagement.dto.AddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.AdminAddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.UserDTO;
import banquemisr.challenge05.TaskManagement.entity.User;
import banquemisr.challenge05.TaskManagement.entity.UserRoleEnum;
import banquemisr.challenge05.TaskManagement.exception.ClientException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final UserDAO userDAO;
    private final ModelMapper modelMapper;
    private final PasswordEncoder encryptionService;


    @Override
    public User getUserByEmail(String email) {
        return userDAO.findByEmail(email).orElseThrow(() -> new ClientException("User not found"));
    }

    @Override
    public User register(AddUserRequest addUserRequest) {
        var user = modelMapper.map(addUserRequest, User.class);
        userDAO.findByEmail(addUserRequest.getEmail()).ifPresent(u -> {
            throw new ClientException("User already exists");
        });
        user.setRole(UserRoleEnum.user);
        user.setPassword(encryptionService.encode(addUserRequest.getPassword()));
        user.setCreatedAt(new Date());
        userDAO.saveUser(user);
        return user;
    }

    @Override
    public Integer addUser(AdminAddUserRequest addUserRequest) {
        var user = modelMapper.map(addUserRequest, User.class);
        userDAO.findByEmail(addUserRequest.getEmail()).ifPresent(u -> {
            throw new ClientException("User already exists");
        });
        user.setPassword(encryptionService.encode(addUserRequest.getPassword()));
        user.setCreatedAt(new Date());
        userDAO.saveUser(user);
        return user.getId();
    }

    @Override
    public List<UserDTO> getUsers() {
        List<User> dbUsers = userDAO.findAll();
        return dbUsers.stream().map(user -> modelMapper.map(user, UserDTO.class)).toList();
    }
}
