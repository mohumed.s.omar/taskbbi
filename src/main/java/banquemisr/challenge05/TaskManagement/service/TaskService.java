package banquemisr.challenge05.TaskManagement.service;

import banquemisr.challenge05.TaskManagement.dto.AddTaskRequest;
import banquemisr.challenge05.TaskManagement.dto.HistoryDTO;
import banquemisr.challenge05.TaskManagement.dto.PaginationTaskResponse;
import banquemisr.challenge05.TaskManagement.dto.UpdateTaskRequest;
import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import banquemisr.challenge05.TaskManagement.entity.User;

import java.time.LocalDateTime;
import java.util.List;

public interface TaskService {


    Integer addTask(AddTaskRequest addTaskRequest, User user);

    void deleteTask(Integer taskId, User user);

    void updateTask(UpdateTaskRequest updateTaskRequest, Integer taskId, User user);




    PaginationTaskResponse getTasks(User user, TaskStatusEnum status,
                                    LocalDateTime fromDueDate, LocalDateTime toDueDate,
                                    TaskPriorityEnum priority, String search,
                                    Integer page, Integer pageSize);

    List<HistoryDTO> getTaskHistory(Integer taskId, User user);

}
