package banquemisr.challenge05.TaskManagement.service;

import banquemisr.challenge05.TaskManagement.dto.AuthenticationResponse;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;

public interface AuthService {

    boolean comparePassword(String password, String hashedPassword);

    String hashPassword(String password);

    public AuthenticationResponse generateToken(UserDetails userDetails);

    public String generateToken(Map<String, Object> claims, UserDetails userDetails);

    boolean validateToken(String token);

    String getEmailFromToken(String token);



}
