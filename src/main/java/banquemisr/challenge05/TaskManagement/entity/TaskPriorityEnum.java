package banquemisr.challenge05.TaskManagement.entity;

public enum TaskPriorityEnum {
    LOW,
    MEDIUM,
    HIGH
}
