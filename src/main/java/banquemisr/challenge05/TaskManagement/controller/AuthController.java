package banquemisr.challenge05.TaskManagement.controller;


import banquemisr.challenge05.TaskManagement.dto.AddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.AuthenticationResponse;
import banquemisr.challenge05.TaskManagement.dto.LoginRequest;
import banquemisr.challenge05.TaskManagement.exception.ClientException;
import banquemisr.challenge05.TaskManagement.service.AuthService;
import banquemisr.challenge05.TaskManagement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {


    @Autowired
    private final UserService userService;
    @Autowired
    private final AuthService authService;

    // login api
    @PostMapping("/login")
    public AuthenticationResponse login(@RequestBody LoginRequest registerRequest) {
        var user = userService.getUserByEmail(registerRequest.getEmail());
        var matchPassword = authService.comparePassword(
                registerRequest.getPassword(), user.getPassword()
        );
        if (matchPassword)
            return authService.generateToken(user);
        else
            throw new ClientException("Invalid Email Or password");
    }


    // add user endpoint
    @PostMapping("/register")
    public AuthenticationResponse addUser(
            @RequestBody AddUserRequest registerRequest
    ) {
        var user = userService.register(registerRequest);
        return authService.generateToken(user);
    }

}
