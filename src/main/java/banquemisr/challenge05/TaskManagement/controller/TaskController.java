package banquemisr.challenge05.TaskManagement.controller;


import banquemisr.challenge05.TaskManagement.controller.annotation.CurrentUser;
import banquemisr.challenge05.TaskManagement.dto.*;
import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import banquemisr.challenge05.TaskManagement.entity.User;
import banquemisr.challenge05.TaskManagement.service.TaskService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@SecurityRequirement(name = "bearerAuth")
@Validated
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;


    @GetMapping
    public String helloWorld() {
        return "Hello World";
    }


    @PostMapping("/add-task")
    public InsertedId addTask(
            @RequestBody
            @Valid
            AddTaskRequest addTaskRequest,
            @Parameter(hidden = true)
            @CurrentUser
            UserDetails currentUser

    ) {
        User user = (User) currentUser;
        System.out.println(currentUser.getUsername());
        var insertedId = taskService.addTask(addTaskRequest, user);

        return InsertedId.builder().insertedId(insertedId).build();
    }


    @DeleteMapping("/delete-task/{taskId}")
    public void deleteTask(
            @PathVariable Integer taskId,
            @Parameter(hidden = true)
            @CurrentUser
            UserDetails currentUser
    ) {
        User user = (User) currentUser;
        taskService.deleteTask(taskId, user);
    }


    @PutMapping("/update-task/{taskId}")
    public void updateTask(
            @PathVariable Integer taskId,
            @RequestBody
            @Valid
            UpdateTaskRequest updateTaskRequest,
            @Parameter(hidden = true)
            @CurrentUser
            UserDetails currentUser
    ) {
        User user = (User) currentUser;
        taskService.updateTask(updateTaskRequest, taskId, user);
    }


    @GetMapping("/get-tasks")
    public PaginationTaskResponse getTasks(
            @RequestParam(required = false) TaskStatusEnum status,
            @RequestParam(required = false) LocalDateTime fromDueDate,
            @RequestParam(required = false) LocalDateTime toDueDate,
            @RequestParam(required = false) TaskPriorityEnum priority,
            @RequestParam(required = false) String search,
            @RequestParam(required = false, defaultValue = "1") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer pageSize,
            @Parameter(hidden = true)
            @CurrentUser
            UserDetails currentUser
    ) {
        User user = (User) currentUser;


        return taskService.getTasks(user, status, fromDueDate, toDueDate, priority, search, page, pageSize);
    }


    @GetMapping("/task-history/{taskId}")
    public List<HistoryDTO> getTaskHistory(
            @PathVariable Integer taskId,
            @Parameter(hidden = true)
            @CurrentUser
            UserDetails currentUser
    ) {
        User user = (User) currentUser;

        return taskService.getTaskHistory(taskId, user);

    }


}
