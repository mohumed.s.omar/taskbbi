package banquemisr.challenge05.TaskManagement.controller;

import banquemisr.challenge05.TaskManagement.dto.AddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.AdminAddUserRequest;
import banquemisr.challenge05.TaskManagement.dto.InsertedId;
import banquemisr.challenge05.TaskManagement.dto.UserDTO;
import banquemisr.challenge05.TaskManagement.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class AdminController {

    private final UserService userService;

    @GetMapping("/create-user")
    public InsertedId create(
            @RequestBody AdminAddUserRequest addUserRequest
    ) {
        var insertedId = userService.addUser(addUserRequest);
        return InsertedId.builder().insertedId(insertedId).build();
    }


    @GetMapping("/users")
    public List<UserDTO> getUsers() {
        return userService.getUsers();
    }

}
