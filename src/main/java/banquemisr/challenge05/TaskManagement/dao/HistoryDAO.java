package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.entity.History;
import banquemisr.challenge05.TaskManagement.entity.Task;

import java.util.List;

public interface HistoryDAO {

   List<History>  getHistoryByTask(Task task);

   void addHistory(History history);

}
