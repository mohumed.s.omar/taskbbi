package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.entity.Task;
import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import banquemisr.challenge05.TaskManagement.entity.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TaskDAO {
    public Integer saveTask(Task task);

    public List<Task> findAll();

    public Optional<Task> findOne(Integer taskId);

    public void updateTask(Task task);


    public void deleteTask(Task task);

    public Long count();

    List<Task> getTasks(
            User user, TaskStatusEnum status,
            LocalDateTime fromDueDate, LocalDateTime toDueDate,
            TaskPriorityEnum priority, String search,
            Integer page, Integer pageSize) ;
}
