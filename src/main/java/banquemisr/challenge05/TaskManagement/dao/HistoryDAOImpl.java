package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.entity.History;
import banquemisr.challenge05.TaskManagement.entity.Task;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;


@RequiredArgsConstructor
@Transactional
@Repository
public class HistoryDAOImpl implements HistoryDAO {

   private final EntityManager entityManager;


   @Override
   public List<History> getHistoryByTask(Task task) {
         var retQuery = entityManager.createQuery("from History where task=:task", History.class)
                .setParameter("task", task);

         return retQuery.getResultList();

   }

   @Override
   public void addHistory(History history) {
         entityManager.persist(history);
   }
}
