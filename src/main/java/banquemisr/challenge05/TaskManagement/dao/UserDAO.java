package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.dto.UserDTO;
import banquemisr.challenge05.TaskManagement.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserDAO {
    Optional<User> findByEmail(String email);

    Optional<User> findById(Integer id);



    public void saveUser(User user);

    List<User> findAll();
}
