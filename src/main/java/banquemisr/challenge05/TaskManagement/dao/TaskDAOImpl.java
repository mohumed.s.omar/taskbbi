package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.entity.Task;
import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import banquemisr.challenge05.TaskManagement.entity.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Repository
@RequiredArgsConstructor
@Transactional
public class TaskDAOImpl implements TaskDAO {


    private final EntityManager entityManager;

    @Override
    public Integer saveTask(Task task) {
        entityManager.persist(task);
        return task.getId();
    }

    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("from Task", Task.class).getResultList();
    }

    @Override
    public Optional<Task> findOne(Integer taskId) {
        return Optional.ofNullable(entityManager.find(Task.class, taskId));
    }

    @Override

    public void updateTask(Task task) {
        entityManager.merge(task);
    }

    @Override
    public void deleteTask(Task task) {
        entityManager.remove(task);
    }

    @Override
    public Long count() {
        return entityManager.createQuery("select count(*) from Task", Long.class).getSingleResult();
    }

    @Override
    public List<Task> getTasks(
            User user, TaskStatusEnum status,
            LocalDateTime fromDueDate, LocalDateTime toDueDate,
            TaskPriorityEnum priority, String search,
            Integer page, Integer pageSize) {


        StringBuilder queryString = new StringBuilder("from Task where creator=:user");


        if (status != null ) {
            queryString.append(" and status=:status");
        }
        if (fromDueDate != null) {
            queryString.append(" and dueDate>=:fromDueDate");
        }
        if (toDueDate != null ) {
            queryString.append(" and dueDate<=:toDueDate");
        }
        if (priority != null) {
            queryString.append(" and priority=:priority");
        }



        if (search != null && !search.isEmpty()) {
            queryString.append(" and (title like :search or description like :search)");
        }


        TypedQuery<Task> query = entityManager.createQuery(queryString.toString(), Task.class)
                .setParameter("user", user);


        if (status != null ) {
            query.setParameter("status", status);
        }
        if (fromDueDate != null) {

            query.setParameter("fromDueDate", fromDueDate);
        }
        if (toDueDate != null ) {
            query.setParameter("toDueDate", toDueDate);
        }
        if (priority != null ) {
            query.setParameter("priority", priority);
        }
        if (search != null && !search.isEmpty()) {
            query.setParameter("search", "%" + search + "%");
        }


        if (page != null && pageSize != null) {
            int firstResult = (page - 1) * pageSize;
            query.setFirstResult(firstResult);
            query.setMaxResults(pageSize);
        }


        return query.getResultList();
    }

}
