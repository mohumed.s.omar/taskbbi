package banquemisr.challenge05.TaskManagement.dao;

import banquemisr.challenge05.TaskManagement.entity.User;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserDAOImpl implements UserDAO {


    private final EntityManager entityManager;

    @Override
    @Transactional
    public Optional<User> findByEmail(String email) {
        var query = entityManager.createQuery("from User where email=:email", User.class);
        query.setParameter("email", email);
        return query.getResultStream().findAny();
    }

    @Override
    public Optional<User> findById(Integer id) {
        return Optional.ofNullable(entityManager.find(User.class, id));
    }

    @Override
    @Transactional
    public void saveUser(User user) {
        entityManager.persist(user);
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("from User", User.class).getResultList();
    }


}
