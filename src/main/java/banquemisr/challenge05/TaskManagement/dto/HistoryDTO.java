package banquemisr.challenge05.TaskManagement.dto;


import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class HistoryDTO {

   private Integer id;
   private TaskStatusEnum status;
   private LocalDateTime createdAt;
}
