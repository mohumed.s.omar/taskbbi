package banquemisr.challenge05.TaskManagement.dto;

import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class UpdateTaskRequest {



    @Size(min = 3, max = 200)
    private String title;
    private String description;
    private LocalDateTime dueDate;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Priority must be provided and valid")
    private TaskPriorityEnum priority;


    @Enumerated(EnumType.STRING)
    @NotNull(message = "Priority must be provided and valid")
    private TaskStatusEnum status;



}
