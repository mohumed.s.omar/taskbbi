package banquemisr.challenge05.TaskManagement.dto;

import banquemisr.challenge05.TaskManagement.entity.TaskPriorityEnum;
import banquemisr.challenge05.TaskManagement.entity.TaskStatusEnum;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TaskDTO {


    private int id;


    private String title;


    private String description;


    private TaskStatusEnum status;


    private TaskPriorityEnum priority;


    private LocalDateTime createdAt;


    private LocalDateTime dueDate;

}
