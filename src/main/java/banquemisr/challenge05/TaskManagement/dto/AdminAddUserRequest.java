package banquemisr.challenge05.TaskManagement.dto;


import banquemisr.challenge05.TaskManagement.entity.UserRoleEnum;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminAddUserRequest {

    @Email
    private String email;
    @Size(min = 8)
    private String password;
    @Size(min = 3, max = 100)
    private String name;


}
