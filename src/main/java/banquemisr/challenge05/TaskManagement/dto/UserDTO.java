package banquemisr.challenge05.TaskManagement.dto;

import banquemisr.challenge05.TaskManagement.entity.UserRoleEnum;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
public class UserDTO {
    private Integer id;
    private String email;
    private UserRoleEnum role;
    private String name;
    private LocalDateTime createdAt;
}
