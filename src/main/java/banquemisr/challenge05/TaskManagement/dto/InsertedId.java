package banquemisr.challenge05.TaskManagement.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class InsertedId {

    private final Integer insertedId;
}
