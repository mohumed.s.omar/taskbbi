package banquemisr.challenge05.TaskManagement.dto;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AuthenticationResponse {
    private final String jwt;
}
