package banquemisr.challenge05.TaskManagement.dto;


import lombok.Data;

@Data
public class LoginRequest {
    private String email;
    private String password;
}
