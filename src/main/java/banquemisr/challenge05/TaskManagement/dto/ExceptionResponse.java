package banquemisr.challenge05.TaskManagement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse {

    public ExceptionResponse(String message, int status) {
        this.message = message;
        this.status = status;
    }

    private String message;
    private int status;
    private List<String> errors;
}
