package banquemisr.challenge05.TaskManagement.dto;


import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PaginationTaskResponse {
    private Integer page;
    private Integer pageSize;
    private List<TaskDTO> data;

}
