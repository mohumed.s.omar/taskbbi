# Spring Boot Tasks Management Service

This is a Spring Boot application for Task Management System.

## Prerequisites

To run this application, you need to have the following installed on your machine:

- Docker
- Docker Compose

## Getting Started



1. Build and run the application using Docker Compose:
    ```sh
    docker-compose up
    ```

   This command will start the application and all its dependencies (e.g., databases, etc.) as defined in the `docker-compose.yml` file.

2. Access the application:

    - Swagger UI: [http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/)

## API Documentation

The API documentation is available via Swagger UI. You can access it at:
[http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/)

[docs json](http://localhost:8080/api-docs)


## throw swagger-ui 
 1. you can use account with email:'admin@gmail.com' and password: '1234'
 2. after this you can create any acc. 
 3. or you can register as norma user.
    4. after you login you will get JWT token.
    5. copy it and put it in the header of the request.
    6. you can do it through swagger-ui. using the button on the top right corner.`autorize`
![img.png](img.png)

![img_1.png](img_1.png)

![img_2.png](img_2.png)

![img_3.png](img_3.png)

![img_4.png](img_4.png)

